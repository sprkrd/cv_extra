
#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

typedef unsigned char byte;

cv::Mat img;

int hsv_tolerance[] = {10,10,10};

void usage();
void color_selection(int,int,int,int,void*);
void obtain_mask(int,void*);

int label_idx = 2;
cv_extra::HSVSegmenter segmenter;

int main(int argc, char* argv[])
{
  if (argc != 2) 
  {
    usage();
    return -1;
  }

  img = cv::imread(argv[1]);

  if (not img.data)
  {
    std::cerr << "image 1: invalid path (" << argv[1] << ")" << std::endl;
    return -1;
  }

  cv::GaussianBlur(img,img,cv::Size(0,0),1.5,1.5);

  cv::namedWindow("Source");
  cv::imshow("Source",img);

  cv::namedWindow("Labeled image");

  cv::createTrackbar("H tolerance","Source",&hsv_tolerance[0],180,obtain_mask);
  cv::createTrackbar("S tolerance","Source",&hsv_tolerance[1],255,obtain_mask);
  cv::createTrackbar("V tolerance","Source",&hsv_tolerance[2],255,obtain_mask);

  cv::setMouseCallback("Source", color_selection, 0);

  obtain_mask(0,0);
  
  while (true)
  {
    char c = (char) cv::waitKey(0);

    if (c == 'q') break;
    else if (c == 'r')
    {
      label_idx = 2;
      segmenter.clear();
      obtain_mask(0,0);
    }
  }
}

void usage()
{
  std::cerr << "Usage: hsv_segmentator path/to/img" << std::endl;
}

void color_selection(int event, int x, int y, int, void*)
{
  if (event != cv::EVENT_LBUTTONDOWN)
    return;

  /* Convert RGB color to HSV and output both RGB and HSV values. */

  cv::Mat color_bgr(1,1,CV_8UC3);
  cv::Mat color_hsv(1,1,CV_8UC3);
  int ch = img.channels();
  for (int i = 0; i < 3; ++i)
    color_bgr.at<byte>(0,i) = img.at<byte>(y,ch*x+i);

  cv::cvtColor(color_bgr,color_hsv,CV_BGR2HSV);
  std::cout << "Color bgr: " << color_bgr << ", ";
  std::cout << "color hsv: " << color_hsv << std::endl;

  cv::Scalar hsv_central_color;
  for (int i = 0; i < 3; ++i)
    hsv_central_color[i] = color_hsv.at<byte>(0,i);

  segmenter[label_idx++] = hsv_central_color;

  obtain_mask(0,0);
}

void obtain_mask(int,void*)
{
  cv::Scalar tolerance(hsv_tolerance[0],hsv_tolerance[1],hsv_tolerance[2]);
  segmenter.set_tolerance(tolerance);
  clock_t start = clock();
  cv::Mat labeled_img = segmenter(img);
  std::cout << "Time elapsed (s): " << (double)(clock()-start)/CLOCKS_PER_SEC
    << std::endl;
  cv::Mat color_label(labeled_img.size(),CV_8UC3,cv::Scalar::all(0));
  for (int i = 0; i < labeled_img.rows; ++i)
  {
    for (int j = 0; j < labeled_img.cols; ++j)
    {
      unsigned char label = labeled_img.at<unsigned char>(i,j);
      if (label > 1)
      {
        const cv::Scalar& s_color = segmenter[label];
        cv::Vec3b b_color(s_color[0],s_color[1],s_color[2]);
        color_label.at<cv::Vec3b>(i,j) = cv_extra::cvtColor(b_color,CV_HSV2BGR);
      }
    }
  }
  cv::namedWindow("Labeled image");
  cv::imshow("Labeled image",color_label);
}
