#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

typedef unsigned char byte;

cv::Mat img,hsv;

int hsv_central_color[] = {0,0,0};
int hsv_tolerance[] = {10,10,10};


void usage();
void color_selection(int,int,int,int,void*);
void obtain_mask(int,void*);

int main(int argc, char* argv[])
{
  if (argc != 2) 
  {
    usage();
    return -1;
  }

  img = cv::imread(argv[1]);

  if (not img.data)
  {
    std::cerr << "image 1: invalid path (" << argv[1] << ")" << std::endl;
    return -1;
  }

  //cv::GaussianBlur(img,img,cv::Size(0,0),1.5,1.5);
  //cv::bilateralFilter(img.clone(),img,9,40,40);
  cv::cvtColor(img, hsv, CV_BGR2HSV);

  cv::namedWindow("Source");
  cv::imshow("Source",img);

  cv::namedWindow("Labels");

  cv::createTrackbar("H","Source",&hsv_central_color[0],255,obtain_mask);
  cv::createTrackbar("S","Source",&hsv_central_color[1],255,obtain_mask);
  cv::createTrackbar("V","Source",&hsv_central_color[2],255,obtain_mask);
  cv::createTrackbar("H tolerance","Source",&hsv_tolerance[0],255,obtain_mask);
  cv::createTrackbar("S tolerance","Source",&hsv_tolerance[1],255,obtain_mask);
  cv::createTrackbar("V tolerance","Source",&hsv_tolerance[2],255,obtain_mask);

  cv::setMouseCallback("Source", color_selection, 0);

  obtain_mask(0,0);

  cv::waitKey(0);
}

void usage()
{
  std::cerr << "Usage: hsv_segmentator path/to/img" << std::endl;
}

double getOrientation(std::vector<cv::Point> &pts, cv::Mat &img)
{
  using namespace std;
  using namespace cv;
  //Construct a buffer used by the pca analysis
  Mat data_pts = Mat(pts.size(), 2, CV_64FC1);
  for (int i = 0; i < data_pts.rows; ++i)
  {
    data_pts.at<double>(i, 0) = pts[i].x;
    data_pts.at<double>(i, 1) = pts[i].y;
  }

  //Perform PCA analysis
  PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);

  //Store the position of the object
  Point pos = Point(pca_analysis.mean.at<double>(0, 0),
      pca_analysis.mean.at<double>(0, 1));

  //Store the eigenvalues and eigenvectors
  vector<Point2d> eigen_vecs(2);
  vector<double> eigen_val(2);
  for (int i = 0; i < 2; ++i)
  {
    eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
        pca_analysis.eigenvectors.at<double>(i, 1));

    eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
  }

  // Draw the principal components
  circle(img, pos, 1, CV_RGB(255, 0, 255), 2);
  line(img, pos, pos + 0.25*Point(eigen_vecs[0].x * eigen_val[0], eigen_vecs[0].y * eigen_val[0]) , CV_RGB(255, 255, 0));
  line(img, pos, pos + 0.25*Point(eigen_vecs[1].x * eigen_val[1], eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 255, 255));

  return atan2(eigen_vecs[0].y, eigen_vecs[0].x);
}

void color_selection(int event, int x, int y, int, void*)
{
  if (event != cv::EVENT_LBUTTONDOWN)
    return;

  cv::Mat color_bgr(1,1,CV_8UC3);
  cv::Mat color_hsv(1,1,CV_8UC3);
  int ch = img.channels();
  for (int i = 0; i < 3; ++i)
    color_bgr.at<byte>(0,i) = img.at<byte>(y, ch*x+i);
  DEBUG(std::cout << "Color bgr: " << color_bgr << ", ";)
  cv::cvtColor(color_bgr, color_hsv,CV_BGR2HSV);
  DEBUG(std::cout << "color hsv: " << color_hsv << std::endl;)
  for (int i = 0; i < 3; ++i)
    hsv_central_color[i] = color_hsv.at<byte>(0,i);

  cv::setTrackbarPos("H","Source",hsv_central_color[0]);
  cv::setTrackbarPos("S","Source",hsv_central_color[1]);
  cv::setTrackbarPos("V","Source",hsv_central_color[2]);

  obtain_mask(0,0);
}

void obtain_mask(int,void*)
{
  cv_extra::HSVSegmenter segmenter;
  cv::Scalar sub(hsv_central_color[0],hsv_central_color[1],
      hsv_central_color[2]);
  cv::Scalar tolerance(hsv_tolerance[0],hsv_tolerance[1],hsv_tolerance[2]);
  std::cout << sub << " " << tolerance << std::endl;
  segmenter[2] = sub;
  segmenter.set_tolerance(tolerance);
  cv::Mat result(hsv.size(), CV_8UC3, cv::Scalar(0));
  std::vector<std::vector<cv::Point> > contours;
  std::vector<int> labels;
  segmenter(hsv, labels, contours, 0, cv::Point(), true);
  for (int i = 0; i < contours.size(); ++i)
  {
    int label = labels[i];
    cv::Vec3b hsv_3b(segmenter[label][0], segmenter[label][1], segmenter[label][2]);
    cv::Scalar color = cv::Scalar(cv_extra::cvtColor(hsv_3b, CV_HSV2BGR));
    cv::drawContours(result, contours, i, color, 1, 8,
        cv::noArray(), 0);
    getOrientation(contours[i], result);
  }
  cv::namedWindow("Labels");
  cv::imshow("Labels", result);
}
