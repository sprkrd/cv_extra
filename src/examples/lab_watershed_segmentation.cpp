#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

typedef unsigned char byte;

int const L1 = 0;
int const L2 = 1;
int const INF = 2;

cv::Mat img,lab,distance;

int lab_central_color[] = {0,0,0};
int weights[] = {100,100,100};
int distance_type = 0;
int threshold = 15;

void usage();
void color_selection(int,int,int,int,void*);
void generate_distance(int,void*);
void obtain_mask(int,void*);

int main(int argc, char* argv[])
{
  if (argc != 2) 
  {
    usage();
    return -1;
  }

  img = cv::imread(argv[1]);

  if (not img.data)
  {
    std::cerr << "image 1: invalid path (" << argv[1] << ")" << std::endl;
    return -1;
  }

  cv::GaussianBlur(img,img,cv::Size(0,0),1.5,1.5);
  cv::cvtColor(img,lab,CV_BGR2Lab);

  cv::namedWindow("Source");
  cv::imshow("Source",img);

  cv::namedWindow("Distance");
  cv::namedWindow("Mask");

  cv::createTrackbar("L*","Source",&lab_central_color[0],255,generate_distance);
  cv::createTrackbar("a*","Source",&lab_central_color[1],255,generate_distance);
  cv::createTrackbar("b*","Source",&lab_central_color[2],255,generate_distance);
  cv::createTrackbar("L* weight","Source",&weights[0],100,generate_distance);
  cv::createTrackbar("a* weight","Source",&weights[1],100,generate_distance);
  cv::createTrackbar("b* weight","Source",&weights[2],100,generate_distance);

  cv::createTrackbar("Distance type","Distance",&distance_type,2,
      generate_distance);

  cv::createTrackbar("Threshold","Mask",&threshold,255, obtain_mask);

  cv::setMouseCallback("Source", color_selection, 0);

  generate_distance(0,0);

  cv::waitKey(0);
}

void usage()
{
  std::cerr << "Usage: lab_segmentator path/to/img" << std::endl;
}

void color_selection(int event, int x, int y, int, void*)
{
  if (event != cv::EVENT_LBUTTONDOWN)
    return;

  cv::Mat color_bgr(1,1,CV_8UC3);
  cv::Mat color_lab(1,1,CV_8UC3);
  int ch = img.channels();
  for (int i = 0; i < 3; ++i)
    color_bgr.at<byte>(0,i) = img.at<byte>(y,ch*x+i);
  DEBUG(std::cout << "Color bgr: " << color_bgr << ", ";)
  cv::cvtColor(color_bgr,color_lab,CV_BGR2Lab);
  DEBUG(std::cout << "color lab: " << color_lab << std::endl;)
  for (int i = 0; i < 3; ++i)
    lab_central_color[i] = color_lab.at<byte>(0,i);

  cv::setTrackbarPos("L*","Source",lab_central_color[0]);
  cv::setTrackbarPos("a*","Source",lab_central_color[1]);
  cv::setTrackbarPos("b*","Source",lab_central_color[2]);

  generate_distance(0,0);
}

void generate_distance(int,void*)
{
  cv::Scalar lab_central_color_f(lab_central_color[0],
    lab_central_color[1],lab_central_color[2]);
  cv::Scalar weights_f(weights[0]/255.0,weights[1]/100.0,weights[2]/100.0);
  if (distance_type==L1)
  {
    cv_extra::WeightedDistance(lab,lab_central_color_f,distance,weights_f,
        cv::NORM_L1);
  }
  if (distance_type == L2)
  {
    cv_extra::WeightedDistance(lab,lab_central_color_f,distance,weights_f,
        cv::NORM_L2,true);
  }
  if (distance_type == INF)
  {
    cv_extra::WeightedDistance(lab,lab_central_color_f,distance,weights_f,
        cv::NORM_INF);
  }
  cv::Mat distance_to_show;
  cv::normalize(distance,distance_to_show,0,1,cv::NORM_MINMAX);
  cv::namedWindow("Distance");
  cv::imshow("Distance",distance_to_show);
  obtain_mask(0,0);
}

void obtain_mask(int,void*)
{
  cv::Mat selected_colors,sure_fg,sure_bg,markers,mask;
  cv::inRange(distance,cv::Scalar(0),cv::Scalar(threshold),selected_colors);
  //cv::Mat strel_opening = cv::getStructuringElement(
      //cv::MORPH_ELLIPSE,cv::Size(3,3));
  //cv::morphologyEx(selected_colors,selected_colors,cv::MORPH_CLOSE,
      //strel_opening);
  //cv::morphologyEx(selected_colors,selected_colors,cv::MORPH_OPEN,
      //strel_opening);
  //cv::imshow("Mask",selected_colors);
  //cv::Mat strel = cv::getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(3,3));
  //cv::morphologyEx(sure_fg,sure_fg,cv::MORPH_CLOSE,strel);
  //cv::morphologyEx(sure_fg,sure_fg,cv::MORPH_OPEN,strel);
  cv::Mat strel_erode_fg = cv::getStructuringElement(
      cv::MORPH_ELLIPSE,cv::Size(3,3));
  cv::Mat strel_erode_bg = cv::getStructuringElement(
      cv::MORPH_ELLIPSE,cv::Size(25,25));
  cv::erode(selected_colors,sure_fg,strel_erode_fg);
  cv::erode(~selected_colors,sure_bg,strel_erode_bg);
  cv::add(sure_fg&1,sure_bg&2,markers,cv::noArray(),CV_32S);
  cv::watershed(lab,markers);
  mask = markers == 1;
  cv::namedWindow("Mask");
  cv::imshow("Mask",mask);
}
