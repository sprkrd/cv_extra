#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

typedef std::vector<cv::Point> Contour;
typedef std::vector<Contour> ContourWrapper;

void drawContour(const std::vector<cv::Point>& contour, cv::Mat& img)
{
  ContourWrapper wrapper(1,contour);
  std::cout << contour.size() << std::endl;
  cv::drawContours(img,wrapper,0,cv::Scalar(255),CV_FILLED);
}

int main(int argc, char* argv[])
{
  assert(argc == 3 and "Usage: shape_matching path/to/template path/to/image");

  cv::Mat temp = cv::imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
  cv::Mat img = cv::imread(argv[2],CV_LOAD_IMAGE_COLOR);

  ContourWrapper contours_temp;
  cv::findContours(temp.clone(),contours_temp,cv::noArray(),CV_RETR_EXTERNAL,
      CV_CHAIN_APPROX_SIMPLE);

  assert(contours_temp.size() == 1);

  cv_extra::ShapeMatcher matcher(contours_temp[0],72);
  
  cv::Mat hsv,shape;
  cv::cvtColor(img,hsv,CV_BGR2HSV);
  cv::inRange(img,cv::Scalar(0,0,0),cv::Scalar(180,255,30),shape);

  ContourWrapper shape_contour;
  cv::findContours(shape.clone(),shape_contour,cv::noArray(),CV_RETR_EXTERNAL,
      CV_CHAIN_APPROX_SIMPLE);

  assert(shape_contour.size() == 1);

  cv::Point2d center;
  double radius;
  cv_extra::centroidEnclosingCircle(shape_contour[0],center,radius);

  std::cout << "(" << center.x << "," << center.y << ") " << radius << std::endl;

  cv_extra::ShapeMatcher::Match match = matcher(shape_contour[0]);

  std::cout << match.similitude << " " << match.optimum_angle << std::endl;

  cv::Mat temp_fit(50,50,CV_8U,cv::Scalar(0));
  drawContour(matcher.get_template_contour(),temp_fit);
  cv::Mat m = cv::getRotationMatrix2D(cv::Point2f(25,25),match.optimum_angle,1);
  cv::warpAffine(temp_fit,temp_fit,m,cv::Size(50,50));

  imshow("Original template",temp);
  imshow("Detected shape",shape);
  imshow("Rotated template",temp_fit);

  cv::waitKey(0);

}
