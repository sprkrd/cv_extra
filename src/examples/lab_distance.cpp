#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

int const L1 = 0;
int const L2 = 1;
int const INF = 2;

cv::Mat lab1,lab2;
cv::Mat distance;

int distance_type = 0;
int threshold = 15;

void usage();
void generate_distance(int,void*);
void obtain_mask(int,void*);


int main(int argc, char* argv[])
{
  if (argc < 3) 
  {
    usage();
    return -1;
  }

  cv::Mat img1 = cv::imread(argv[1]);
  cv::Mat img2 = cv::imread(argv[2]);

  if (not img1.data)
  {
    std::cerr << "image 1: invalid path (" << argv[1] << ")" << std::endl;
    return -1;
  }

  if (not img2.data)
  {
    std::cerr << "image 2: invalid path (" << argv[2] << ")" << std::endl;
    return -1;
  }

  assert(img1.type() == CV_8UC3 and cv_extra::equal_size_and_type(img1,img2));

  cv::imshow("Source 1",img1);
  cv::imshow("Source 2",img2);

  cv::cvtColor(img1,lab1,CV_BGR2Lab);
  cv::cvtColor(img2,lab2,CV_BGR2Lab);

  cv::namedWindow("Distance");
  cv::namedWindow("Mask");

  cv::createTrackbar("Distance type","Distance",&distance_type,2,
      generate_distance);
  cv::createTrackbar("Threshold","Distance",&threshold,50, obtain_mask);

  generate_distance(0,0);

  cv::waitKey(0);
}

void usage()
{
  std::cerr << "Usage: lab_distance path/to/img1 path/to/img2" << std::endl;
}

void generate_distance(int,void*)
{
  if (distance_type==L1)
  {
    cv_extra::WeightedDistance(lab1,lab2,distance,cv::Scalar(100.0/255.0,1,1),
        cv::NORM_L1);
  }
  if (distance_type == L2)
  {
    cv_extra::WeightedDistance(lab1,lab2,distance,cv::Scalar(100.0/255.0,1,1),
        cv::NORM_L2,true);
  }
  if (distance_type == INF)
  {
    cv_extra::WeightedDistance(lab1,lab2,distance,cv::Scalar(100.0/255.0,1,1),
        cv::NORM_INF);
  }
  cv::Mat distance_to_show;
  cv::normalize(distance,distance_to_show,0,1,cv::NORM_MINMAX);
  cv::namedWindow("Distance");
  cv::imshow("Distance",distance_to_show);
  obtain_mask(0,0);
}

void obtain_mask(int,void*)
{
  cv::Mat mask;
  cv::inRange(distance,cv::Scalar(0),cv::Scalar(threshold),mask);
  cv::namedWindow("Mask");
  cv::imshow("Mask",mask);
}
