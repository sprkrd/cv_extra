#include "cv_extra.h"
#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <cassert>

typedef unsigned char byte;

cv::Mat img,hsv;

int hsv_central_color[] = {0,0,0};
int hsv_tolerance[] = {10,10,10};

void usage();
void color_selection(int,int,int,int,void*);
void obtain_mask(int,void*);

int main(int argc, char* argv[])
{
  if (argc != 2) 
  {
    usage();
    return -1;
  }

  img = cv::imread(argv[1]);

  if (not img.data)
  {
    std::cerr << "image 1: invalid path (" << argv[1] << ")" << std::endl;
    return -1;
  }

  cv::GaussianBlur(img,img,cv::Size(0,0),1.5,1.5);
//   cv::bilateralFilter(img.clone(),img,9,40,40);
  cv::cvtColor(img,hsv,CV_BGR2HSV);

  cv::namedWindow("Source");
  cv::imshow("Source",img);

  cv::namedWindow("Mask");

  cv::createTrackbar("H","Source",&hsv_central_color[0],255,obtain_mask);
  cv::createTrackbar("S","Source",&hsv_central_color[1],255,obtain_mask);
  cv::createTrackbar("V","Source",&hsv_central_color[2],255,obtain_mask);
  cv::createTrackbar("H tolerance","Source",&hsv_tolerance[0],255,obtain_mask);
  cv::createTrackbar("S tolerance","Source",&hsv_tolerance[1],255,obtain_mask);
  cv::createTrackbar("V tolerance","Source",&hsv_tolerance[2],255,obtain_mask);

  cv::setMouseCallback("Source", color_selection, 0);

  obtain_mask(0,0);

  cv::waitKey(0);
}

void usage()
{
  std::cerr << "Usage: hsv_segmentator path/to/img" << std::endl;
}

void color_selection(int event, int x, int y, int, void*)
{
  if (event != cv::EVENT_LBUTTONDOWN)
    return;

  cv::Mat color_bgr(1,1,CV_8UC3);
  cv::Mat color_hsv(1,1,CV_8UC3);
  int ch = img.channels();
  for (int i = 0; i < 3; ++i)
    color_bgr.at<byte>(0,i) = img.at<byte>(y,ch*x+i);
  DEBUG(std::cout << "Color bgr: " << color_bgr << ", ";)
  cv::cvtColor(color_bgr,color_hsv,CV_BGR2HSV);
  DEBUG(std::cout << "color hsv: " << color_hsv << std::endl;)
  for (int i = 0; i < 3; ++i)
    hsv_central_color[i] = color_hsv.at<byte>(0,i);

  cv::setTrackbarPos("H","Source",hsv_central_color[0]);
  cv::setTrackbarPos("S","Source",hsv_central_color[1]);
  cv::setTrackbarPos("V","Source",hsv_central_color[2]);

  obtain_mask(0,0);
}

void obtain_mask(int,void*)
{
  cv::Scalar sub(hsv_central_color[0],hsv_central_color[1],
      hsv_central_color[2]);
  cv::Scalar threshold(hsv_tolerance[0],hsv_tolerance[1],hsv_tolerance[2]);
  cv::Mat diff,mask;
  cv_extra::HsvAbsDiff(hsv,sub,diff);
  cv::inRange(diff,cv::Scalar(0,0,0),threshold,mask);
  cv::Mat strel = cv::getStructuringElement(cv::MORPH_ELLIPSE,cv::Size(3,3));
  cv::morphologyEx(mask,mask,cv::MORPH_CLOSE,strel);
  cv::morphologyEx(mask,mask,cv::MORPH_OPEN,strel);
  cv::namedWindow("Mask");
  cv::imshow("Mask",mask);
}
