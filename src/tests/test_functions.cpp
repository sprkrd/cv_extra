/* disable asserts in cv_extra */
#ifndef NDEBUG
#define NDEBUG
#endif

#include <iostream>
#include "functions.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE Functions
#include <boost/test/unit_test.hpp>


typedef unsigned char byte;
double const epsilon = 1E-2;

bool equal_float(cv::Mat mat1, cv::Mat mat2)
{
  cv::Mat diff;
  cv::absdiff(mat1,mat2,diff);
  return not cv::countNonZero(diff.reshape(1,1)>epsilon);
}

bool equal(cv::Mat mat1, cv::Mat mat2)
{
  cv::Mat inequalities = mat1!=mat2;
  return not cv::countNonZero(inequalities.reshape(1,1));
}

BOOST_AUTO_TEST_CASE(HsvAbsDiff_1)
{
  byte data1[] = {
    5,70,100, 120,5,5,
    160,70,100, 100,0,0,
    180,0,0, 91,0,0,
  };
  cv::Mat mat1(3,2,CV_8UC3,data1);

  byte data2[] = {
    175,75,95, 115,255,255,
    20,65,105, 105,100,100,
    0,0,0, 0,0,0
  };
  cv::Mat mat2(3,2,CV_8UC3,data2);

  byte data_correct_result[] = {
    10,5,5, 5,250,250,
    40,5,5, 5,100,100,
    0,0,0, 89,0,0
  };
  cv::Mat correct(3,2,CV_8UC3,data_correct_result);

  cv::Mat hsv_abs_diff;
  cv_extra::HsvAbsDiff(mat1,mat2,hsv_abs_diff);

  BOOST_REQUIRE(hsv_abs_diff.type() == CV_8UC3);

  BOOST_CHECK_MESSAGE(equal(hsv_abs_diff,correct),
      "HsvAbsDiff between\n"<<mat1<<"\nand\n"<<mat2<<"\nreturned\n"
      <<hsv_abs_diff<<"\nCorrect result is\n"<<correct);
}

BOOST_AUTO_TEST_CASE(HsvInRange_1)
{
  byte data[] = {
    5,50,100, 50,50,100, 170,50,100,
    5,200,200, 50,200,200, 170,200,200
  };
  cv::Mat mat(2,3,CV_8UC3,data);

  byte correct_mask_data[] = {
    255,0,255,
    0,0,0
  };
  cv::Mat correct_mask(2,3,CV_8U,correct_mask_data);

  cv::Scalar min(160,40,90);
  cv::Scalar max(10,60,100);

  cv::Mat mask;
  cv_extra::HsvInRange(mat,min,max,mask);

  BOOST_REQUIRE(mask.type() == CV_8UC1);

  BOOST_CHECK_MESSAGE(equal(mask,correct_mask),"HsvInRange for\n"<<mat<<
      "\nwith min="<<min<<", max="<<max<<"; returned\n"<<mask<<
      "\nCorrect result\n"<<correct_mask);
}

BOOST_AUTO_TEST_CASE(HsvInRange_2)
{
  byte data[] = {
    5,50,100, 50,50,100, 170,50,100,
    5,200,200, 50,200,200, 170,200,200
  };
  cv::Mat mat(2,3,CV_8UC3,data);

  byte correct_mask_data[] = {
    0,255,255,
    0,255,255
  };
  cv::Mat correct_mask(2,3,CV_8U,correct_mask_data);

  cv::Scalar min(20,50,50);
  cv::Scalar max(180,255,255);

  cv::Mat mask;
  cv_extra::HsvInRange(mat,min,max,mask);

  BOOST_REQUIRE(mask.type() == CV_8UC1);

  BOOST_CHECK_MESSAGE(equal(mask,correct_mask),"HsvInRange for\n"<<mat<<
      "\nwith min="<<min<<", max="<<max<<"; returned\n"<<mask<<
      "\nCorrect result\n"<<correct_mask);
}

BOOST_AUTO_TEST_CASE(PerPixelNorm_1)
{
  byte data[] = {
    1,1,2, 3,5,8, 13,21,34,
    1,0,0, 0,1,1, 1,1,1,
    0,0,0, 2,8,8, 255,255,255
  };
  cv::Mat mat(3,3,CV_8UC3,data);

  float data_norm_L1[] = {
    4, 16, 68,
    1, 2, 3,
    0, 18, 765
  };
  cv::Mat L1_correct(3,3,CV_32F,data_norm_L1);

  float data_norm_L2_squared[] = {
    6, 98, 1766,
    1, 2, 3,
    0, 132, 195075
  };
  cv::Mat L2_squared_correct(3,3,CV_32F,data_norm_L2_squared);

  float data_norm_L2[] = {
    2.45, 9.9, 42.02,
    1, 1.41, 1.73,
    0, 11.49, 441.67
  };
  cv::Mat L2_correct(3,3,CV_32F,data_norm_L2);

  float data_norm_inf[] = {
    2, 8, 34,
    1, 1, 1,
    0, 8, 255
  };
  cv::Mat inf_correct(3,3,CV_32F,data_norm_inf);

  cv::Mat norm_L1;
  cv_extra::PerPixelNorm(mat,norm_L1,cv::NORM_L1);
  BOOST_CHECK_MESSAGE(equal_float(L1_correct,norm_L1),
      "L1 norm calculated for\n"<<mat<<"\nis\n"<<norm_L1
      <<"\nCorrect result\n"<<L1_correct);

  cv::Mat norm_L2_sq;
  cv_extra::PerPixelSqL2Norm(mat,norm_L2_sq);
  BOOST_CHECK_MESSAGE(equal_float(L2_squared_correct,norm_L2_sq),
      "SQ L2 norm calculated for\n"<<mat<<"\nis\n"<<norm_L2_sq
      <<"\nCorrect result\n"<<L2_squared_correct);

  cv::Mat norm_L2;
  cv_extra::PerPixelNorm(mat,norm_L2);
  BOOST_CHECK_MESSAGE(equal_float(L2_correct,norm_L2),
      "L1 norm calculated for\n"<<mat<<"\nis\n"<<norm_L2
      <<"\nCorrect result\n"<<L2_correct);

  cv::Mat norm_inf;
  cv_extra::PerPixelNorm(mat,norm_inf,cv::NORM_INF);
  BOOST_CHECK_MESSAGE(equal_float(inf_correct,norm_inf),
      "L1 norm calculated for\n"<<mat<<"\nis\n"<<norm_inf
      <<"\nCorrect result\n"<<inf_correct);

}

BOOST_AUTO_TEST_CASE(WeightedDistance_1)
{
  byte data1[] = {
    0,0,0, 255,100,100,
    27,27,27, 10,20,30
  };
  cv::Mat mat1(2,2,CV_8UC3,data1);

  byte data2[] = {
    255,255,255, 0,100,100,
    1,1,1, 30,20,10
  };
  cv::Mat mat2(2,2,CV_8UC3,data2);

  cv::Scalar weights(100.0/255.0,1,1);

  float data_L1_correct[] = {
    610, 100,
    62.2, 27.84
  };
  cv::Mat L1_correct(2,2,CV_32F,data_L1_correct);

  float data_sq_L2_correct[] = {
    140050, 10000,
    1455.96, 461.5148
  };
  cv::Mat sq_L2_correct(2,2,CV_32F,data_sq_L2_correct);

  cv::Mat L1_dist;
  cv_extra::WeightedDistance(mat1,mat2,L1_dist,weights,cv::NORM_L1);
  BOOST_CHECK_MESSAGE(equal_float(L1_correct,L1_dist),
      "L1 dist calculated for\n"<<mat1<<"\nand\n"<<mat2<<"\nis\n"<<L1_dist
      <<"\nCorrect result\n"<<L1_correct);

  cv::Mat sq_L2_dist;
  cv_extra::WeightedDistance(mat1,mat2,sq_L2_dist,weights,cv::NORM_L2);
  BOOST_CHECK_MESSAGE(equal_float(sq_L2_correct,sq_L2_dist),
      "SQ L2 dist calculated for\n"<<mat1<<"\nand\n"<<mat2<<"\nis\n"
      <<sq_L2_dist<<"\nCorrect result\n"<<sq_L2_correct);

}
