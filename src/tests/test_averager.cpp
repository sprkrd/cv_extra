/* disable asserts in cv_extra */
#ifndef NDEBUG
#define NDEBUG
#endif

#include <iostream>
#include "averager.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TemporalAverager
#include <boost/test/unit_test.hpp>


typedef unsigned char byte;
double const epsilon = 1E-2;

bool equal(cv::Mat mat1, cv::Mat mat2)
{
  cv::Mat inequalities = mat1!=mat2;
  return not cv::countNonZero(inequalities.reshape(1,1));
}

BOOST_AUTO_TEST_CASE(TemporalAverager_1)
{
  byte data1[] = {
    8,2,3,2,3,7,7,9,5,
    8,9,8,1,7,7,9,4,6,
    6,0,2,3,1,8,4,2,3
  };
  cv::Mat mat1(3,3,CV_8UC3,data1);

  byte data2[] = {
    9,1,0,5,2,9,3,3,4,
    6,1,2,7,3,4,3,7,9,
    1,2,9,3,5,7,2,2,1
  };
  cv::Mat mat2(3,3,CV_8UC3,data2);

  byte data3[] = {
    1,8,2,6,3,1,9,9,2,
    3,7,3,9,3,4,4,1,2,
    8,7,6,3,4,1,2,4,8
  };
  cv::Mat mat3(3,3,CV_8UC3,data3);

  cv_extra::TemporalAverager averager(2);
  
  cv::Mat correct1 = mat1;
  cv::Mat res1 = averager(mat1);
  BOOST_REQUIRE(res1.type() == CV_8UC3);
  BOOST_CHECK_MESSAGE(equal(res1,correct1),
      "res:\n"<<res1<<"\ncorrect:\n"<<correct1);

  cv::Mat correct2 = (mat1+mat2)/2;
  cv::Mat res2 = averager(mat2);
  BOOST_REQUIRE(res2.type() == CV_8UC3);
  BOOST_CHECK_MESSAGE(equal(res2,correct2),
      "res:\n"<<res2<<"\ncorrect:\n"<<correct2);

  cv::Mat correct3 = (mat2+mat3)/2;
  cv::Mat res3 = averager(mat3);
  BOOST_REQUIRE(res3.type() == CV_8UC3);
  BOOST_CHECK_MESSAGE(equal(res3,correct3),
      "res:\n"<<res3<<"\ncorrect:\n"<<correct3);
}
