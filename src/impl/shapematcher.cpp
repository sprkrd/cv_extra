/**
 *  \file shapematcher.cpp
 *  \author Alejandro Suarez Hernandez
 *  Implementation of the ShapeMatcher class.
 */
#include <opencv2/imgproc/imgproc.hpp>

#include "functions.h"
#include "shapematcher.h"

#include <opencv2/highgui/highgui.hpp>

namespace
{
  const double PI = 3.14159265359;
}

namespace cv_extra
{

ShapeMatcher::ShapeMatcher(const std::vector<cv::Point>& template_contour,
    double angle_limit, int cmp_radius, double simplify_error_factor) : 
  template_contour_(1), angle_limit_(angle_limit), cmp_radius_(cmp_radius), 
  simplify_error_factor_(simplify_error_factor)
{
  /* Here we scale and simplify the template contour. */
  std::vector<cv::Point> fit_template, approx_template;
  scaleAndShift(template_contour,fit_template,
      cv::Rect(0,0,2*cmp_radius_,2*cmp_radius_));
  double arc_len = cv::arcLength(template_contour,true);
  cv::approxPolyDP(fit_template,approx_template,
      simplify_error_factor*arc_len,true);
  template_contour_[0] = approx_template;
}

ShapeMatcher::Match ShapeMatcher::operator()(
    const std::vector<cv::Point>& contour,double angle_resolution) const
{
  std::vector<cv::Point> fit_contour;
  scaleAndShift(contour,fit_contour,
      cv::Rect(0,0,2*cmp_radius_,2*cmp_radius_));

  /* Contours must be wrapped in a vector in order to use the drawContours
   * function from OpenCV. */
  _ContourWrapper contour_w(1,fit_contour);

  cv::Mat shape(2*cmp_radius_,2*cmp_radius_,CV_8U,cv::Scalar(0));
  cv::drawContours(shape,contour_w,0,cv::Scalar(255),CV_FILLED);

  /* We create canvas and rotated_contour outside of the for loop so
   * OpenCV can reutilize the already allocated space. */
  cv::Mat canvas(2*cmp_radius_,2*cmp_radius_,CV_8U);
  _ContourWrapper rotated_contour(1);
  cv::Point2f center(cmp_radius_,cmp_radius_);

  const int MAX_ERROR = cvRound(PI*cmp_radius_*cmp_radius_);
  int min_n_errors = MAX_ERROR;
  double optimum_angle;

  /* Compare for each angle between 0 ang angle_limit at a resolution of
   * angle_resolution. */
  for (double angle=0; angle < angle_limit_; angle += angle_resolution)
  {
    canvas = cv::Scalar(0);
    cv::Mat m = cv::getRotationMatrix2D(center,angle,1);
    cv::transform(template_contour_[0],rotated_contour[0],m);
    cv::drawContours(canvas,rotated_contour,0,cv::Scalar(255),CV_FILLED);
    cv::imshow("canvas",canvas^shape);
    int error = cv::countNonZero(canvas^shape);
    if (error < min_n_errors)
    {
      /* Update optimum angle. */
      min_n_errors = error;
      optimum_angle = angle;
    }
  }

  Match match;
  match.similitude = 1.0-((double)min_n_errors)/MAX_ERROR;
  match.optimum_angle = optimum_angle;
  return match;
}

}
