#include "averager.h"

namespace cv_extra
{

void TemporalAverager::removeExcess()
{
  /* Remove all the images that do not fit into the averager's memory */
  while (stored_images_.size() > memory_)
  {
    cv::Mat oldest = stored_images_.front();
    stored_images_.pop();
    cv::subtract(sum_,oldest,sum_,cv::noArray(),CV_16U);
  }
}

TemporalAverager::TemporalAverager(int memory) : memory_(memory) {}

cv::Mat TemporalAverager::operator()(const cv::Mat& image)
{
  CV_Assert(image.depth() == CV_8U and
      "Depths other than CV_8U are not supported");

  if (sum_.rows != image.rows or sum_.cols != image.cols
      or sum_.channels() != image.channels())
  {
    /* First image, or image with different dimensions.
     * Clear queue and set sum_ accordingly. */
    stored_images_ = std::queue<cv::Mat>();
    sum_ = cv::Mat(image.size(),CV_16UC(image.channels()),cv::Scalar::all(0));
  }

  /* Sum image to sum_ and push it into the averager's queue. */
  cv::add(sum_,image,sum_,cv::noArray(),CV_16U);
  stored_images_.push(image);
  
  removeExcess();

  /* Compute and return average. */
  cv::Mat average;
  cv::divide(sum_,cv::Scalar::all(stored_images_.size()),average,1,
        image.depth());
  return average;
}

}
