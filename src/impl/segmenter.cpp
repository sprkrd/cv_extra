/* \file segmenter.cpp
 * \author Alejandro Suarez Hernandez
 * \brief Implementation of the classes from segmenter.h
 */
#include <opencv2/imgproc/imgproc.hpp>
#include "segmenter.h"
#include "functions.h"

namespace cv_extra
{

HSVSegmenter::HSVSegmenter(int bg_erosion_radius, int fg_erosion_radius)
{
  /* Background erosion structuring element */
  bg_strel_ = cv::getStructuringElement(cv::MORPH_ELLIPSE,
      cv::Size(bg_erosion_radius*2+1,bg_erosion_radius*2+1));

  /* Foreground erosion structuring element */
  fg_strel_ = cv::getStructuringElement(cv::MORPH_ELLIPSE,
      cv::Size(fg_erosion_radius*2+1,fg_erosion_radius*2+1));
}

cv::Mat HSVSegmenter::operator()(const cv::Mat& image, bool ishsv) const
{
  typedef std::map<int,cv::Scalar>::const_iterator Iterator;

  /* Convert to HSV if necessary */
  cv::Mat hsv;
  if (not ishsv) cv::cvtColor(image,hsv,CV_BGR2HSV);
  else hsv = image;

  /* Select all the pixels that fall inside the range defined by the
   * specified central colors and the tolerances. */
  cv::Mat labeled_img(image.size(),CV_8U,cv::Scalar(0));
  cv::Mat diff, sure_fg, sure_bg, markers;

  for (Iterator it = colors_.begin(); it != colors_.end(); ++it)
  {
    /* Compute absolute difference and select the pixels that fall inside
     * the specified tolerance. Add the resulting mask to labeled_img. */
    int label = it->first;
    const cv::Scalar& color = it->second;
    HsvAbsDiff(hsv,color,diff);
    for (int i = 0; i < diff.rows; ++i)
    {
      for (int j = 0; j < diff.cols; ++j)
      {
        const cv::Vec3b& px_diff = diff.at<cv::Vec3b>(i,j);
        bool select = px_diff[0] < tolerance_[0] and px_diff[1] < tolerance_[1]
          and px_diff[2] < tolerance_[2];
        if (select) labeled_img.at<unsigned char>(i,j) = label;
      }
    }
  }

  /* Obtain sure background and sure foreground via erosion. Background will
   * have label 1. */
  cv::erode(labeled_img,sure_fg,fg_strel_);
  cv::erode((labeled_img==0)&1,sure_bg,bg_strel_);

  /* Combine both background and foreground in the same image with different
   * labels. The image must be CV_32S for the OpenCV watershed method. */
  cv::add(sure_fg,sure_bg,markers,cv::noArray(),CV_32S);

  /* Improve the segmentation with the watershed algorithm */
  cv::watershed(image,markers);

  /* Convert markers to CV_8U type. Edges (-1) will now have 0 as label.
   * Background's label will continue having label 1.*/
  markers.convertTo(labeled_img, CV_8U);

  return labeled_img;
}

void HSVSegmenter::operator()(const cv::Mat& image, std::vector<int>& labels,
    std::vector<std::vector<cv::Point> >& contours, int min_area,
    cv::Point offset, bool ishsv) const
{
  typedef std::map<int,cv::Scalar>::const_iterator Iterator;

  cv::Mat segmented_image = (*this)(image,ishsv);
  for (Iterator it = colors_.begin(); it != colors_.end(); ++it)
  {
    int label = it->first;

    cv::Mat eq_label = (segmented_image == label);

    /* Obtain the contours of the blobs from this label. */
    std::vector<std::vector<cv::Point> > contours_label;
    cv::findContours(eq_label,contours_label,cv::noArray(),CV_RETR_EXTERNAL,
        CV_CHAIN_APPROX_SIMPLE, offset);

    for (int i = 0; i < contours_label.size(); ++i)
    {
      /* Push the contour to contours only if it is large enough.  */
      if (cv::contourArea(contours_label[i]) >= min_area)
      {
        contours.push_back(contours_label[i]);
        labels.push_back(label);
      }
    }
  }
}

RegionSegmenter::RegionSegmenter(const cv::Point& center, int inclusive_radius,
    int exclusive_radius) :
  center_(center), inclusive_radius_(inclusive_radius),
  exclusive_radius_(exclusive_radius) {}

cv::Mat RegionSegmenter::operator()(const cv::Mat& image) const
{
  cv::Mat markers(image.size(),CV_32S,cv::Scalar(1));
  /* Unmark pixels inside exclusive circumference. */
  cv::circle(markers,center_,exclusive_radius_,cv::Scalar(0),CV_FILLED);
  /* Mark pixels inside inclusive circumference. */
  cv::circle(markers,center_,inclusive_radius_,cv::Scalar(2),CV_FILLED);
  cv::watershed(image,markers);
  return (markers == 2);
}

void RegionSegmenter::operator()(const cv::Mat& image,
    std::vector<std::vector<cv::Point> >& contours) const
{
  cv::Mat segmented_image = (*this)(image);
  cv::findContours(segmented_image,contours,cv::noArray(),CV_RETR_EXTERNAL,
      CV_CHAIN_APPROX_SIMPLE);
  /* The method should detect just one contour. */
  CV_Assert(contours.size() == 1);
}

} /* end namespace */
