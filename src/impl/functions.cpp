/**
 *  \file functions.cpp
 *  \brief Definitions for each function of functions.h
 *  \author Alejandro Suarez Hernandez
 */

#include <cmath>

#include "functions.h"

namespace cv_extra
{

cv::Scalar cvtColor(cv::Scalar color, int conversion, int nchannels,
    int depth)
{
  /* Build a matrix with a single position equal to color. */
  cv::Mat color_mat(1,nchannels,depth), cvt_color_mat;
  color_mat = color_mat.reshape(nchannels,1);
  color_mat = color;

  cv::cvtColor(color_mat,cvt_color_mat,conversion);
  cvt_color_mat.convertTo(cvt_color_mat,CV_64F);

  cv::Scalar outcolor(0,0,0,0);
  for (int i = 0; i < nchannels; ++i)
    outcolor[i] = cvt_color_mat.at<double>(1,i);

  return outcolor;
}

void HsvAbsDiff(const cv::Mat& img1, cv::InputArray sub, cv::Mat& dst)
{
  CV_Assert(img1.channels() == 3 and
      "img1 must have exactly 3 channels (H,S,V)");

  cv::absdiff(img1,sub,dst);

  int hmax = dst.depth()==CV_8U? 180 : 360;

  for (int i = 0; i < dst.rows; ++i)
  {
    for (int j = 0; j < dst.cols; ++j)
    {
      /* Take into account the cyclic nature of the Hue. */
      unsigned char* ptr = &dst.data[i*dst.step+3*j];
      unsigned char h_abs_diff = *ptr;
      if (hmax-h_abs_diff < h_abs_diff) *ptr = hmax-h_abs_diff;
    }
  }
}

void HsvInRange(const cv::Mat& src, const cv::Scalar& min,
    const cv::Scalar& max, cv::Mat& dst)
{
  CV_Assert(src.channels() == 3 and
      "the input matrix must have exactly 3 channels");

  if (min[0] > max[0])
  {
    /* temporary matrix */
    cv::Mat tmp;

    /* First step: hue from min[0] to HMAX */
    cv::Scalar max_1 = max;
    max_1[0] = 180;
    cv::inRange(src,min,max_1,tmp);

    /* Second step: hue from 0 to max[0] */
    cv::Scalar min_2 = min;
    min_2[0] = 0;
    cv::inRange(src,min_2,max,dst);

    dst = dst | tmp;
  }
  else
  {
    cv::inRange(src,min,max,dst);
  }
}

void PerPixelNorm(const cv::Mat& src, cv::Mat& dst, int normType,
    bool normalize)
{
  int rows = src.rows;
  int cols = src.cols;
  dst = cv::Mat(rows*cols,1,CV_32F);

  /* In tmp, each column represents a channel and each row a pixel. */
  cv::Mat tmp = src.isContinuous()? src : src.clone();
  tmp = tmp.reshape(1,rows*cols);

  /* Compute norm for each pixel */
  for (int i = 0; i < rows*cols; ++i)
  {
    dst.at<float>(i) = cv::norm(tmp.row(i),normType);
  }

  /* Set dst's shape to that of src */
  dst = dst.reshape(1,rows);

  if (normalize) cv::normalize(dst,dst,0,1,cv::NORM_MINMAX);
}

void PerPixelSqL2Norm(const cv::Mat& src, cv::Mat& dst, bool normalize)
{
  int rows = src.rows;
  int cols = src.cols;
  /* Compute components squares */
  cv::Mat tmp;
  cv::multiply(src,src,tmp,1,CV_32F);

  /* Reshape tmp so each column represents a channel and each row a pixel. */
  tmp = tmp.reshape(1,rows*cols);

  /* Sum columns. The result is the squared distances. */
  cv::reduce(tmp,dst,1,CV_REDUCE_SUM,CV_32F);

  /* Reshape dst in order to make it have the same dimensions as src */
  dst = dst.reshape(1,rows);
  if (normalize) cv::normalize(dst,dst,0,1,cv::NORM_MINMAX);
}

void WeightedDistance(const cv::Mat& img1, cv::InputArray sub, cv::Mat& dst,
   cv::Scalar weights, int normType, bool sqrt, bool normalize)
{
  /* Compute abs difference */
  cv::Mat diff,weighted_diff;
  cv::absdiff(img1,sub,diff);

  /* Multiply differences by weights */
  cv::multiply(diff,weights,weighted_diff,1,CV_32F);

  /* Compute norms */
  if (normType == cv::NORM_L2 and not sqrt)
  {
    cv_extra::PerPixelSqL2Norm(weighted_diff,dst,normalize);
  }
  else
  {
    cv_extra::PerPixelNorm(weighted_diff,dst,normType,normalize);
  }
}

void scaleAndShift(cv::InputArray input, cv::OutputArray output,
    const cv::Rect& rect)
{
  /* NOTE! In this function we have chosen to compute the centroid enclosing
   * circle. The minimum enclosing circle would also be a good option. */

  /* Compute centroid enclosing circle */
  cv::Point2d center;
  double radius;
  centroidEnclosingCircle(input,center,radius);

  /* Compute scale/shift matrix in order to make input fit into rect */
  double sx = rect.width/(2*radius);
  double sy = rect.height/(2*radius);
  double m_data[] = {
    sx, 0,  rect.x+rect.width/2-sx*center.x,
    0,  sy, rect.y+rect.height/2-sy*center.y
  };
  cv::Mat m(2,3,CV_64F,m_data);

  /* Transforms input point cloud in order to fit in rect */
  cv::transform(input,output,m);
}

void centroidEnclosingCircle(cv::InputArray input, cv::Point2d& center,
    double& radius)
{
  /* Set center to contour's centroid */
  cv::Moments moments = cv::moments(input);
  center.x = moments.m10/moments.m00;
  center.y = moments.m01/moments.m00;

  /* Calculate translation vector of each contour point to centroid*/
  cv::Mat delta = input.getMat() - cv::Scalar(center.x,center.y);

  /* Compute square distances */
  cv::Mat dstsq;
  PerPixelSqL2Norm(delta,dstsq);

  /* Set radius to maximum distance. */
  cv::minMaxIdx(dstsq,NULL,&radius);
  radius = std::sqrt(radius);
}

void principalDirections(const std::vector<cv::Point>& contour, cv::Vec2d& fst,
    cv::Vec2d& snd, bool as_row)
{
  cv::PCA pca(contour, cv::noArray(),
      as_row? CV_PCA_DATA_AS_ROW : CV_PCA_DATA_AS_COL);
  fst = pca.eigenvectors.at<cv::Vec2d>(0);
  snd = pca.eigenvectors.at<cv::Vec2d>(1);
}

} /* end namespace */
