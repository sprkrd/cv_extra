/**
 *  \file shapematcher.h
 *  \author Alejandro Suarez Hernandez
 *  Header for ShapeMatcher class
 */
#ifndef _SHAPEMATCHER_H_
#define _SHAPEMATCHER_H_

#include <opencv2/core/core.hpp>
#include <vector>

namespace cv_extra
{

/**
 *  \brief ShapeMatcher class
 *
 *  Instances of this class store the simplified contour of a template shape.
 *  Other contours are compared to this shape. The main purpose of this class
 *  is to give the optimum rotation angle for the template so it is most
 *  similar to the other contour, and a measure of how similar they are.
 */
class ShapeMatcher
{
  private:
    /* Contours must be wrapped in a vector in order to use the drawContours
     * function from OpenCV. */
    typedef std::vector<std::vector<cv::Point> > _ContourWrapper; 

    _ContourWrapper template_contour_;
    double angle_limit_;
    int cmp_radius_;
    double simplify_error_factor_;

  public:

    /* \brief Match structure
     *
     * This structure is used for the return value of the function
     * operator ().
     */
    struct Match
    {
      double similitude;
      double optimum_angle;
    };

    /*  \brief Constructor
     *
     *  ShapeMatcher constructor
     *
     *  \param template_contour Contour of the template
     *  \param angle_limit Only the angles between 0 and angle_limit are
     *  considered. If the template is symmetrical, this parameter can
     *  be set to a value less than 360 deg for a greater efficiency.
     *  \param cmp_radius Both the template and the compared shapes are scaled
     *  so they fit and can rotate freely in a canvas of dimensions
     *  (2*cmp_radius) x (2*cmp_radius).
     *  \param simplify_error_factor The templates are simplified in order to
     *  generate less points and perform the comparison operation quicker.
     *  The higher this value, the more agressive the simplification (if 0,
     *  no simplification is performed).
     *
     */
    ShapeMatcher(const std::vector<cv::Point>& template_contour,
        double angle_limit=360, int cmp_radius=25,
        double simplify_error_factor_=1E-2);

    /* \brief Comparison between the template and other shape
     *
     * The purpose of the function operator is to perform the comparison
     * between the stored template and other shape. The comparison algorithm
     * is as follows: the given contour is drawed in a canvas.
     * The template is rotated and drawed in a canvas for
     * several rotation angles. The resulting images are xor-ed in order
     * to find the differences between the pair. The optimum rotation angle
     * is the one that minimizes the number of errors.
     *
     * \param contour This contour is compared with the stored template
     *
     * \param angle_resolution Only angles between 0 and angle_limit (see
     * constructor) at a resolution of angle_resolution are considered.
     *
     * \return Match instance. This structure contains: nerrors, the number
     * of differences at the optimum rotation angle; optimum_angle, the
     * rotation angle that minimizes the number of differences.
     *
     */
    Match operator()(const std::vector<cv::Point>& contour,
        double angle_resolution=1.0) const;

    /**
     *  \brief Accessor to template contour
     *
     *  \return Template contour as a vector of Point instances.
     */
    const inline std::vector<cv::Point>& get_template_contour() const
    {
      return template_contour_[0];
    }

};

} /* end namespace */

#endif
