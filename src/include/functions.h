/**
 *  \file functions.h
 *  \brief Utility functions builded on top of OpenCV functionality.
 *
 *  This header contains the declaration of utility functions. These provide
 *  extra functionality that it is not directly provided by OpenCV.
 *
 *  \author Alejandro Suarez Hernandez
 */

#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include <vector>
#include <opencv2/opencv.hpp>


namespace cv_extra
{

/**
 *  \brief Image size and type comparison
 *  \param img1 First image
 *  \param img2 Second image
 *  \return true iff img1 size and type are equal to img2 size and type
 */
bool inline equal_size_and_type(const cv::Mat& img1, const cv::Mat& img2)
{
  return img1.type() == img2.type() and img1.rows == img2.rows and
    img2.cols == img2.cols;
}

/**
 *  \brief HSV abs difference
 *
 *  In OpenCV there already is a method that calculates the absolute
 *  difference between two images. However, there is no method that takes into
 *  account the particularity of the H channel in HSV images. Since H is
 *  cyclic, it has more sense to give min(abs(H1-H2),HMAX-abs(H1-H2)). This is
 *  the purpose of this method. For S and V, the absolute difference is just
 *  the regular one.
 *
 *  \param img1 First image. Must have 3 channels.
 *  \param sub  This element will be substracted from img1. It may be another
 *              image or a scalar.
 *  \param dst  The absolute difference shall be left here. The method supports
 *              in-place operations (i.e. dst may be img1 or img2).
 */
void HsvAbsDiff(const cv::Mat& img1, cv::InputArray sub, cv::Mat& dst);

/**
 *  \brief inRange function for HSV images
 *
 *  Since the H channel in HSV images is cyclic, it has sense to select all
 *  the pixels with an H range between H1 and H2 with H1 > H2. This means
 *  that we select all the hue values in the range [H1,HMAX] and [0,H2].
 *  However, OpenCV's inRange method would return a mask with all the positions
 *  set to zero. This method takes the H channel into consideration and checks
 *  whether it has sense to apply the regular inRange method or apply a
 *  special treatment. The other channels (saturation and value) are treated
 *  normally.
 *
 *  \param src  HSV image. This method only supports the CV_8UC3 type.
 *  \param min  Begining of the interval.
 *  \param max  End of the interval. The cyclic nature of H is taken into
 *              account. S and V are treated normally.
 *  \param dst  The resulting mask
 */
void HsvInRange(const cv::Mat& src, const cv::Scalar& min,
    const cv::Scalar& max, cv::Mat& dst);

/**
 *  \brief Calculates the norm of each pixel's color.
 *
 *  \param src  The method will calculate the norm of each pixel from src. Each
 *              channel is interpreted as a vector component.
 *  \param dst  Output array. The method will store here the results. Each
 *              distance is represented with a float, so dst will end being of
 *              type CV_32FC1.
 *  \param normType   May be one of: cv::NORM_INF, cv::NORM_L2, cv::NORM_L1
 *  \param normalize  Tells if the method has to scale and shift the distances
 *                    so that they fit in the [0.0, 1.0] range.
 */
void PerPixelNorm(const cv::Mat& src, cv::Mat& dst, int normType=cv::NORM_L2,
    bool normalize=false);

/**
 *  \brief Calculates the squared L2 norm per pixel.
 *
 *  Similar to PerPixelNorm. However, this method only calculates the squared
 *  L2 norm. This is more efficient since there is no need to calculate square
 *  roots. The user should prefer this method if the square distance is enough
 *  for their purpose.
 *
 *  \param src Input image
 *  \param dst Squared L2 norms.
 *  \param normalize  Tells if the method has to scale and shift the distances
 *                    so that they fit in the [0.0, 1.0] range.
 */
void PerPixelSqL2Norm(const cv::Mat& src, cv::Mat& dst, bool normalize=false);

/**
 *  \brief Euclidean distance between two images or an image and a color
 *
 *  This method calculates the weighted euclidean distance between the pixels
 *  of equal-size images or between the colors in an image and a single color.
 *
 *  \param img1 First image
 *  \param sub  The distances shall be calculated from img1 to this element.
 *              It may be a single color (a scalar) or an equal size and type
 *              image.
 *  \param dst  Output image. The distances shall be stored here as a CV_32F
 *              matrix
 *  \param weights  Weights of each channel. It is allowed to assign different
 *                  weights to each channel.
 *  \param normType Distance type. The following types are allowed:
 *                  cv::NORM_L1, cv::NORM_L2, cv::NORM_INF
 *  \param sqrt If normType is cv::NORM_L2, this parameter indicates if the
 *              method has to return the distances (sqrt=true) or the squared
 *              distances (sqrt=false). sqrt=false should be preferred for
 *              efficiency if the squared distances are enough.
 *  \param normalize  Tells if the method has to scale and shift the distances
 *                    so that they fit in the [0.0, 1.0] range.
 */
void WeightedDistance(const cv::Mat& img1, cv::InputArray sub, cv::Mat& dst,
    cv::Scalar weights=cv::Scalar(1,1,1), int normType=cv::NORM_L2,
    bool sqrt=false, bool normalize=false);

/**
 *  \brief Scales and shifts a contour in order to fit in a certain area
 *
 *  Scales and shift a contour so it fits in a certain area (delimited
 *  by rect) and can rotate freely with respect to the center of such area.
 *
 *  \param input Input contour
 *  \param output Scaled and shifted output contour
 *  \param rect The function will scale and shift the input contour so it fits
 *  inside this rectangle and so it can rotate freely with respect to its
 *  center.
 */
void scaleAndShift(cv::InputArray input, cv::OutputArray output,
    const cv::Rect& rect);

/**
 *  \brief Calculates the centroid enclosing circle of a contour
 *
 *  It returns the circle centered in the centroid of the given contour that
 *  encloses the whole contour.
 *
 *  \param input Input contour or point cloud
 *  \param center The centroid of input shall be stored here
 *  \param radius The radius of the enclosing circle shall be stored here
 */
void centroidEnclosingCircle(cv::InputArray input, cv::Point2d& center,
    double& radius);

void principalDirections(const std::vector<cv::Point>& contour, cv::Vec2d& fst,
    cv::Vec2d& snd, bool as_row);



/**
 *  \brief Convenience template methods
 *  
 *  Obtains the type of the matrix from the type (template parameter) and the
 *  number of channels
 *  
 *  \param nchannels Number of channels
 *
 *  \return integer representing matrix type.
 *  */
template<typename _Tp>
inline int type(int nchannels) {CV_Assert(false and "Invalid type");}

template<>
inline int type<unsigned char>(int nchannels){return CV_8UC(nchannels);}

template<>
inline int type<char>(int nchannels){return CV_8SC(nchannels);}

template<>
inline int type<unsigned short int>(int nchannels){return CV_16UC(nchannels);}

template<>
inline int type<short int>(int nchannels){return CV_16SC(nchannels);}

template<>
inline int type<int>(int nchannels){return CV_32SC(nchannels);}

template<>
inline int type<float>(int nchannels){return CV_32FC(nchannels);}

template<>
inline int type<double>(int nchannels){return CV_64FC(nchannels);}


/**
 *  \brief Convenience method for converting a single color.
 *
 *  Converts a single color to another color space.
 *
 *  \param color Color to convert
 *
 *  \retrun converted color
 */
template<typename _Tp, int n>
inline cv::Vec<_Tp,n> cvtColor(const cv::Vec<_Tp,n>& color, int conversion)
{
  /* Create new 1x1 matrix and initialize it to color. */
  cv::Mat color_mat(1,1,type<_Tp>(n));
  color_mat.at<cv::Vec<_Tp,n> >(0,0) = color;

  /* Convert and return converted color */
  cv::Mat cvt_color_mat;
  cv::cvtColor(color_mat,cvt_color_mat,conversion);

  /* Store color in a Vec<_Tp,n> and return it. */
  cv::Vec<_Tp,n> outcolor;
  for (int i = 0; i < cvt_color_mat.channels(); ++i)
    outcolor[i] = cvt_color_mat.at<_Tp>(0,i);
  return cvt_color_mat.at<cv::Vec<_Tp,n> >(0,0);
}

}



#endif

