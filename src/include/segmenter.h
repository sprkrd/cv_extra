/**
 * \file segmenter.h
 * \author Alejandro Suarez Hernandez
 * \brief Header file for segmenter classes
 */

#ifndef _SEGMENTER_H_
#define _SEGMENTER_H_

#include <map>
#include <opencv2/core/core.hpp>

namespace cv_extra
{

/*
 * \brief Image segmenter.
 *
 * Uses color selection in HSV space + watershed algorithm for fine tuning.
 * It accepts several colors and three tolerance values (one per channel).
 * Each color is associated to a label. The segmenter works as follows.
 *  - Transforms a BGR image to HSV
 *  - Selects the pixels that fall inside a range determined by each of the
 *  colors and the tolerance. The selected pixels are marked with the label
 *  of the color they belong to.
 *  - Obtains a matrix of markers eroding the foreground (all the selected
 *  pixels) and the background (the non selected pixels).
 *  - Uses the watershed algorithm with the original (non converted) image
 *  and the markers that were computed in the previous step.
 *  - (Optional) Obtains the contours of each of the blobs detected in
 *  the previous step.
 */
class HSVSegmenter
{
  private:
    std::map<int,cv::Scalar> colors_;
    cv::Scalar tolerance_;
    cv::Mat fg_strel_, bg_strel_;

  public:

    /*
     * \brief Constructor
     *
     * Creates a new HSVSegmenter
     *
     * \param bg_erosion_radius The "sure background" mask for watershed
     * markers is obtained eroding the non selected pixels by this quantity.
     *
     * \param fg_erosion_radius The "sure foreground" mask for watershed
     * markers is obtained eroding the selected areas by this quantity.
     *
     */
    HSVSegmenter(int bg_erosion_radius=12, int fg_erosion_radius=1);

    /*
     * \brief Color accessor/modifier
     *
     * Access or modifies the color associated to a label. If the label does
     * not have any color, then a new one is linked to it. IMPORTANT: labels
     * 0 and
     *
     * \param label
     */
    inline cv::Scalar& operator[](int label)
    {
      CV_Assert(label > 1 and "Labels 0 and 1 reserved.");
      return colors_[label];
    }

    /* \brief Modifier
     *
     * Sets the tolerance of the segmenter to the initial HSV color selector
     * to this quantity.
     * 
     * \param tolerance New tolerance.
     */
    inline void set_tolerance(const cv::Scalar& tolerance)
    {
      tolerance_ = tolerance;
    }

    /*
     * \brief Accessor
     *
     * Gives the current tolerance of the segmenter.
     *
     * \return Segmenter's HSV tolerance for initial color selection.
     */
    inline const cv::Scalar& get_tolerance() const {return tolerance_;}

    /*
     *  \brief Reset colors
     *
     *  Erases all the stored colors and their associated labels.
     */
    inline void clear() {colors_.clear();}

    /*
     * \brief Main segmentation method.
     *
     * Segments the given image. The result is a 8-bit single channel image
     * in which each pixel is marked with one of the following:
     *  - 0 if it belongs to an edge
     *  - 1 if it belongs to the background
     *  - A label associated to one of the colors of the segmenter.
     *
     * \param image BGR image.
     *
     * \return Single channel 8-bit labeled image.
     */
    cv::Mat operator()(const cv::Mat& image, bool ishsv=false) const;

    /*
     * \brief Segmentation method. Returns contours instead of an image.
     *
     * Similar to the single argument function operator. Instead of returning
     * a labeled image, it gives the contours of the detected blobs and the
     * labels associated to each blob.
     *
     * \param image BGR image
     * \param labels The method will store here the labels of each contour.
     * \param contours Contours of the detected blobs.
     * \param min_area Only the contours whose area is at least min_area are
     * stored in contours.
     */
    void operator()(const cv::Mat& image, std::vector<int>& labels,
        std::vector<std::vector<cv::Point> >& contours, int min_area=0,
        cv::Point offset = cv::Point(), bool ishsv=false) const;

};

/*
 * Class for local segmentation of a region in an image. The main method
 * requires the center of the region of interest, an exclusive radius (pixels
 * farther than this radius from the center will be considered part of the
 * background) and an inclusive radius (pixels nearer than this radius
 * from the center will be considered part of the element of interest).
 * The main method takes an image as argument and works as follows:
 *  - Obtains an image of markers drawing a filled circle of radius
 *  inclusive_radius at the given center. A different marker is assigned to
 *  the pixels farther than exclusive_radius from the given center.
 *  - Runs the watershed algorithm with the given image and the markers
 *  computed in the previous step.
 */
class RegionSegmenter
{
  private:
    cv::Point center_;
    int inclusive_radius_;
    int exclusive_radius_;

  public:

    /*
     * \brief Default constructor
     *
     * Creates a new RegionSegmenter instance.
     */
    RegionSegmenter() {}

    /*
     * \brief Constructor
     *
     * Creates a new RegionSegmenter instance with the given parameters.
     *
     * \param center Center of the region of intereset.
     * \param inclusive_radius Check class description.
     * \param exclusive_radius Check class description.
     */
    RegionSegmenter(const cv::Point& center, int inclusive_radius,
        int exclusive_radius);

    /*
     * \brief Modifier
     *
     * Modifies center parameter.
     *
     * \param center New center.
     */
    inline void set_center(const cv::Point& center) {center_ = center;}

    /*
     * \brief Modifier
     *
     * Modifies inclusive_radius parameter.
     *
     * \param inclusive_radius New inclusive_radius.
     */
    inline void set_inclusive_radius(int inclusive_radius)
    {
      inclusive_radius_ = inclusive_radius;
    }

    /*
     * \brief Modifier
     *
     * Modifies exclusive_radius parameter.
     *
     * \param exclusive_radius New exclusive_radius.
     */
    inline void set_exclusive_radius(int exclusive_radius)
    {
      exclusive_radius_ = exclusive_radius;
    }

    /*
     * \brief Accessor
     *
     * \return Center of the region of interest.
     */
    inline const cv::Point& get_center() const {return center_;}

    /*
     * \brief Accessor
     *
     * \return inclusive_radius of the segmenter.
     */
    inline int get_inclusive_radius() const {return inclusive_radius_;}

    /*
     * \brief Accessor
     *
     * \return exclusive_radius of the segmenter.
     */
    inline int get_exclusive_radius() const {return exclusive_radius_;}

    /*
     * \brief Main segmentation method.
     *
     * Returns a binary 8-bit single channel image. The non-zero pixels are
     * the selected shape while the zero pixels belong to the background
     *
     * \param image Input image (BGR and L*a*b* work best).
     * 
     * \return Labeled image.
     */
    cv::Mat operator()(const cv::Mat& image) const;

    /*
     * \brief Segmentation method.
     *
     * It returns the contour of the detected blob instead of a labeled image.
     *
     * \param image Input image
     * \param contour The contour is stored in this parameter.
     *
     * \return Contour of the detected blob.
     */
    void operator()(const cv::Mat& image,
        std::vector<std::vector<cv::Point> >& contour) const;
    
};

}

#endif
