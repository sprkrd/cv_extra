/**
 *  \file cv_extra.h
 *  \brief Main header file of the library
 *
 *  This is a convenience header that internally includes all the headers
 *  of the library.
 *
 *  \author Alejandro Suarez Hernandez
 */

#ifndef _CV_EXTRA_H
#define _CV_EXTRA_H

#include "functions.h"
#include "averager.h"
#include "shapematcher.h"
#include "segmenter.h"

/**
 *  \brief Library namespace
 *
 *  All the library methods and classes are defined inside this namespace.
 */
namespace cv_extra {}

#endif
