/**
 *  \file averager.h
 *  \brief TemporalAverager class header
 *
 *  This header declares a class for averaging sequences of images.
 *
 *  \author Alejandro Suarez Hernandez
 */
#ifndef _AVERAGER_H_
#define _AVERAGER_H_

#include <opencv2/core/core.hpp>
#include <queue>

namespace cv_extra
{

/**
 *  \brief Class for averaging sequences of images.
 *
 *  Each time the function operator of an instance of this class is invoked on
 *  an image, it performs the average of this image with all the previously
 *  stored ones. When the memory of the filter is exceeded, it discards the
 *  oldest image.
 *
 *  The purpose of this class is to serve as a denoising technique.
 */
class TemporalAverager
{
  private:
    int memory_;
    cv::Mat sum_;
    std::queue<cv::Mat> stored_images_;

    void removeExcess();

  public:

    /**
     *  \brief Constructor
     *
     *  Creates a new instance of TemporalAverager
     *
     *  \param memory Memory of the averager. It will average the last
     *  memory images each time the function operator is invoked.
     */
    TemporalAverager(int memory=20);

    /**
     *  \brief Averager (function operator)
     *
     *  Averages the given image with the latest images stored by this filter.
     *
     *  \param image next image to store internally in this instance. This
     *  image is used to calculate the average. If the memory of the averager
     *  is exceeded, the oldest image is removed.
     *
     *  \return averaged image
     */
    cv::Mat operator()(const cv::Mat& image);

    /**
     *  \brief Accessor to the averager's memory
     *
     *  \return memory of the averager.
     */
    inline int get_memory() const {return memory_;}

    /**
     *  \brief Modifier of the averager's memory
     *
     *  Modifies the averager's memory.
     *
     *  \param memory New memory of the averager. If it is smaller than the
     *  previous memory value and the inner storage structure contains more
     *  than memory images, the oldest images are removed.
     */
    inline void set_memory(int memory)
    {
      memory_ = memory;
      removeExcess();
    }

};

}

#endif
