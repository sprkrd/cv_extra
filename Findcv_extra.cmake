#edit the following line to add the librarie's header files
FIND_PATH(cv_extra_INCLUDE_DIR cv_extra.h /usr/include/iridrivers/cv_extra /usr/local/include/iridrivers/cv_extra)

FIND_LIBRARY(cv_extra_LIBRARY
    NAMES cv_extra
    PATHS /usr/lib /usr/local/lib /usr/local/lib/iridrivers/cv_extra) 

IF (cv_extra_INCLUDE_DIR AND cv_extra_LIBRARY)
   SET(cv_extra_FOUND TRUE)
ENDIF (cv_extra_INCLUDE_DIR AND cv_extra_LIBRARY)

IF (cv_extra_FOUND)
   IF (NOT cv_extra_FIND_QUIETLY)
      MESSAGE(STATUS "Found cv_extra: ${cv_extra_LIBRARY}")
   ENDIF (NOT cv_extra_FIND_QUIETLY)
ELSE (cv_extra_FOUND)
   IF (cv_extra_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find cv_extra")
   ENDIF (cv_extra_FIND_REQUIRED)
ENDIF (cv_extra_FOUND)

